import java.util.*

/*
    * NOM: Erik Vasilyan
    * DATA: 2023-03-13
    * TITOL: Divideix o Cero
 */

fun main() {
    val scanner = Scanner(System.`in`)

    val dividend = scanner.nextInt()
    val divisor = scanner.nextInt()

    val numero = divideixoCero(dividend, divisor)

    println(numero)
}

fun divideixoCero(dividend: Int, divisor: Int): Int {

    val resultat: Int = try {
        dividend / divisor
    } catch (e: ArithmeticException) {
        0
    }

    return resultat
}