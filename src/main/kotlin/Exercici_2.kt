import java.util.*

/*
    * NOM: Erik Vasilyan
    * DATA: 2023-03-13
    * TITOL: Pasar a Double o 1.0
 */

fun main() {
    val scanner = Scanner(System.`in`)

    val numero = scanner.next()

    val numeroDouble = aDoubleoU(numero)

    println(numeroDouble)
}

fun aDoubleoU(numero: Any): Double {

    val resultat: Double = try {
        numero.toString().toDouble()
    } catch (e: NumberFormatException) {
        1.0
    }

    return resultat
}