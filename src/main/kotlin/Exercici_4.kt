import java.lang.UnsupportedOperationException

/*
    * NOM: Erik Vasilyan
    * DATA: 2023-03-13
    * TITOL: Aplicació lliure
 */

fun main() {

    var productes = mutableListOf("ou", "oli", "sal", "pebre", "pasta")
    productes.removeAll { true }

    try {
        val producteInexistent = productes.first()
        println(producteInexistent)

    } catch (e: NoSuchElementException) {
        println("Error: ${e.message}")
    }

    val immutableList = listOf("sucre", "vinagre")
    productes = immutableList as MutableList<String>

    try {
        productes.remove("sucre")
    } catch (e: UnsupportedOperationException) {
        println("Error: ${e.message}")
    }
}