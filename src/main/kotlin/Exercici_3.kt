import java.io.File
import java.io.IOException

/*
    * NOM: Erik Vasilyan
    * DATA: 2023-03-13
    * TITOL: Llegir un fitxer
 */

fun main() {
    val fitxer = File("fitxerInexistent.txt")

    try {
        val data = fitxer.readText()
        println(data)
    } catch (e: IOException) {
        println("S'ha produït un error d'entrada/sortida: ${e.message}")
    }
}